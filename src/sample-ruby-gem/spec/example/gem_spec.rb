# frozen_string_literal: true

RSpec.describe Example::Gem do
  it "has a version number" do
    expect(Example::Gem::VERSION).not_to be nil
  end
end
